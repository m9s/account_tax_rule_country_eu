# The COPYRIGHT file at the top level of this repository contains
# the full copyright notices and license terms.
from .test_account_tax_rule_country_eu import suite

__all__ = ['suite']
