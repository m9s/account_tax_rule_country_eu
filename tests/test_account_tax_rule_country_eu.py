# The COPYRIGHT file at the top level of this repository contains
# the full copyright notices and license terms.
import unittest


from trytond.tests.test_tryton import ModuleTestCase
from trytond.tests.test_tryton import suite as test_suite


class AccountTaxRuleCountryEuTestCase(ModuleTestCase):
    'Test Account Tax Rule Country Eu module'
    module = 'account_tax_rule_country_eu'


def suite():
    suite = test_suite()
    suite.addTests(unittest.TestLoader().loadTestsFromTestCase(
            AccountTaxRuleCountryEuTestCase))
    return suite
