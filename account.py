# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.

from trytond.pool import Pool, PoolMeta
from trytond.model import fields
from trytond.transaction import Transaction

__all__ = ['TaxRuleLineTemplate', 'TaxRuleLine', 'InvoiceLine']

TYPES = [
    (None, ''),
    ('goods', 'Goods'),
    ('service', 'Services'),
    ]
LEGAL_FORMS = [
    (None, ''),
    ('person', 'Person (without VAT ID)'),
    ('company', 'Company (with VAT ID)'),
    ]


class TaxRuleLineTemplate:
    __metaclass__ = PoolMeta
    __name__ = 'account.tax.rule.line.template'
    type = fields.Selection(TYPES, 'Type')
    legal_form = fields.Selection(LEGAL_FORMS,
        'Legal Form of Party (VAT Specification)')
    comment = fields.Char('Comment')

    def _get_tax_rule_line_value(self, rule_line=None):
        value = super(TaxRuleLineTemplate, self)._get_tax_rule_line_value(
            rule_line=rule_line)
        if not rule_line or rule_line.type != self.type:
            value['type'] = (
                self.type.id if self.type else None)
        if not rule_line or rule_line.legal_form != self.legal_form:
            value['legal_form'] = (
                self.legal_form.id if self.legal_form else None)
        return value


class TaxRuleLine:
    __metaclass__ = PoolMeta
    __name__ = 'account.tax.rule.line'
    type = fields.Selection(TYPES, 'Type')
    legal_form = fields.Selection(LEGAL_FORMS,
            'Legal Form of Party (VAT Specification)')
    comment = fields.Char('Comment')

    @staticmethod
    def default_country():
        pool = Pool()
        Company = pool.get('company.company')
        Location = pool.get('stock.location')

        warehouses = Location.search([
                ('type', '=', 'warehouse'),
                ])
        if len(warehouses) == 1:
            warehouse = warehouses[0]
            if warehouse.address and warehouse.address.country:
                return warehouse.address.country.id

        company = Company(Transaction().context.get('company'))
        delivery_address = company.party.address_get(type='delivery')
        if delivery_address:
            return delivery_address.country.id


class InvoiceLine:
    __metaclass__ = PoolMeta
    __name__ = 'account.invoice.line'

    def _get_tax_rule_pattern(self):
        pool = Pool()
        SaleLine = pool.get('sale.line')
        PurchaseLine = pool.get('purchase.line')

        pattern = super(InvoiceLine, self)._get_tax_rule_pattern()

        type, legal_form = None, None
        if (self.product and
                self.product.type in [i[0] for i in TYPES]):
            type = self.product.type
        if isinstance(self.origin, SaleLine):
            legal_form = self.origin.sale.shipment_address.party.legal_form
        elif isinstance(self.origin, PurchaseLine):
            legal_form = self.origin.purchase.invoice_address.party.legal_form

        pattern['type'] = type
        pattern['legal_form'] = legal_form
        return pattern
