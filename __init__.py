# The COPYRIGHT file at the top level of this repository contains
# the full copyright notices and license terms.
from trytond.pool import Pool
from .account import *
from .sale import *
from .party import *
from .purchase import *

def register():
    Pool.register(
        TaxRuleLineTemplate,
        TaxRuleLine,
        InvoiceLine,
        Party,
        SaleLine,
        PurchaseLine,
        module='account_tax_rule_country_eu', type_='model')
